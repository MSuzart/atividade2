package br.edu.ucsal.atividade2;


import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class TesteParametro {
	
	@Parameters(name="{index} - calcfat({0})={1}")
	public static Collection<Object[]> obterCasosTeste() {
		return Arrays.asList(new Object[][] { { 0, 1 }, { 1, 1 }, { 2, 2 }, { 3, 6 }, { 4, 24 }, { 5, 120 }, { 6, 720 }, { 7, 5040 }, { 8, 40320 }, { 9, 362880 } });
	}
	
	@Parameter
	public Integer num;
	
	@Parameter(1)
	public Integer resultFat;

	@Test
	public void testarFatorialParametrizado() {
		Integer fat = App.calcFat(num);


		Assert.assertEquals(resultFat, fat);
	}



}

package br.edu.ucsal.atividade2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class TesteSaida {
	
	
	@Test
	public void teste120() {

	ByteArrayOutputStream out = new ByteArrayOutputStream();
	System.setOut(new PrintStream(out));


	App.exibirRespostaFat(120);
	String resultadoAtual = out.toString();

	String resultadoEsperado = "Fatorial = 120";
	Assert.assertEquals(resultadoEsperado, resultadoAtual);
	}
}

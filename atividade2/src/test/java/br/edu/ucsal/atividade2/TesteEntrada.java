package br.edu.ucsal.atividade2;

import java.io.ByteArrayInputStream;

import org.junit.Assert;
import org.junit.Test;

public class TesteEntrada {
	
	
	@Test
	public void test5() {

		
	ByteArrayInputStream in = new ByteArrayInputStream("5".getBytes());

	
	System.setIn(in);
	int resultadoAtual = App.exibirPergunta();
	int resultadoEsperado = 5;
	Assert.assertEquals(resultadoEsperado, resultadoAtual);

}
}

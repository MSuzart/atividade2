package br.edu.ucsal.atividade2;

import java.util.Scanner;

public class App {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		Integer num;
		Integer fat;
		
		num = exibirPergunta();
		
		fat = calcFat(num);
		
		exibirRespostaFat(fat);
	}

	public static int calcFat(int num) {
		if (num == 1 || num == 0) {
			return 1;
		} else {
			return calcFat(num - 1) * num;
		}

	}

	public static Integer exibirPergunta() {
		Integer num;
		
		do {
			System.out.println("Informe um número de 1 a 100");
			num = sc.nextInt();
			return num;
		} while (num <= 100 && num >= 0);

	}
	
	static void exibirRespostaFat(Integer n) {
		
		System.out.println("Fatorial = "+n);

	}
}
